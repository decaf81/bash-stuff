#!/bin/bash
LOGFILE="/var/log/backup.log"
BACKUP_DIR="/backups"
ISNFS="0" #If set to 1, script will exit with an error if Targetpath is not a mountpoint
TIMESTAMP=$(date +%F)
TARGETPATH="$BACKUP_DIR/$HOSTNAME/$TIMESTAMP" #Default target path
STATEFILENAME=".sqlbackup.state"
STATEFILE="$TARGETPATH/$STATEFILENAME"
KEEP_DAYS=1 #Default retention
e="mysql,performance_schema,information_schema" #Default excludes

####ZABBIX SPECIFIC SETTINGS
USEZABBIX="y"
ITEMNAME="sqlbackup.status" #SET item name here
JSONHOSTNAME="lampdoos.example.loc" #SET zabbix agentd hostname here
ZABBIXSERVER=zabbix2.example.loc #Hostname or IP zabbix server or zabbix proxy
INFOSTRING=""

## MORE TUNING ETC
CLEANINDIVIDUALFILES="0"
#SELECTIVE_BACKUP=0
#DO_ALL=0

function monitoring {
  if [ -z $JSONHOSTNAME ]; then
    JSONHOSTNAME=$(echo $HOSTNAME)
  fi
if [ $USEZABBIX == "y" ]; then
  exec 2>/dev/null 3<>/dev/tcp/$ZABBIXSERVER/10051 2>/dev/null #Try to connect to zabbix server
  if [ ! $? -eq '0' ]; then                                    #
    errormsg "Could not connect to Zabbix server"              # Error out if unable to connect
    return                                                     #
  fi

  # Send a command over the connection
  echo -e "{\"request\":\"sender data\",\"data\":[{\"host\":\"$JSONHOSTNAME\",\"key\":\"$ITEMNAME\",\"value\":\"$1\"}]}" >&3 2>/dev/null

  # Try to do someting with the response of the server 
  RESPONSE=$(cat <&3)
    if [[ $RESPONSE = *success* ]]; then
        if [[ ! $RESPONSE = *"failed: 0"* ]];then
	           errormsg "MONITORING: Zabbix gave us a failed response"
	else
		   msg "Sent message to Zabbix"
		   return
        fi
    else
	      errormsg "MONITORING:Zabbix found a syntax error in our json"
    fi
else
    msg "Zabbix notification disabled, so not trying it"
fi
}



function msg {
  if [ -f $LOGFILE ]; then
    echo $(date) : $1 >> $LOGFILE
  fi
  if [ $VERBOSE == 1 ]; then
    echo $(date) : $1
  fi
}

function errormsg {
  if [ -f $LOGFILE ]; then
    echo ERR: $(date) : $1 >> $LOGFILE
  fi
  echo ERR: $(date) : $1
}

function checknfs {
  cat /proc/mounts | grep $BACKUP_DIR
  if [ ! $? = '0' ]; then
    errormsg "$BACKUP_DIR is not a nfs mount."
    monitoring "FAILED: $BACKUP_DIR is not a nfs mount."
    exit 1
  fi
}


function check_mycnf()
{
  if [ ! -f ~/.my.cnf ]; then
    errormsg "Didnt find a .my.cnf. in homedir"
    exit 1

  fi
}

function backup_db()
{
  checkdest
  DATABASE=$1
  TARGET=$TARGETPATH/$DATABASE.$TIMESTAMP.$(date +%H_%M_%S).sql$EXT
  msg "Starting backup: $DATABASE"
  OUTPUT=$((mysqldump $SQLPARAM $DATABASE > $TARGET) 2>&1  )
  if [ ! ${PIPESTATUS[0]} -eq 0 ]; then
    errormsg "Error opening DB $1"
    errormsg $OUTPUT
    rm -f $TARGET
    ERRDB="$ERRDB $DATABASE"
    ((error++))
  else
    msg "Finished DB $1"
  fi
  cleanup

}

function cleanup {
  #remove old files
  msg "Starting cleanup phase"
  if [ $CLEANINDIVIDUALFILES = '1' ]; then
  	CLEANUPFILES=$(find $BACKUP_DIR/$HOSTNAME -type f -name "$DATABASE*.sql$EXT" -mtime +$KEEP_DAYS)
  	for i in $CLEANUPFILES; do
  		msg "Deleting $i"
		rmdiropts=""
  	done
  else
  	rmdiropts="-rf" 
  fi
  #find $BACKUP_DIR/$HOSTNAME -type d -empty -exec rm -rf {} \;
  #cleanup directory
  STATEDIRS=$(find $BACKUP_DIR/$HOSTNAME -name .sqlbackup.state -mtime +$KEEP_DAYS -execdir pwd \;)
  for i in $STATEDIRS; do
	msg "Cleanup $STATEDIRS"
	rm $rmdiropts $i 2>/dev/null
	if [ $? -eq '0' ]; then
		msg "Deleted $i"
	else
		errormsg "Error deleting $i, check dir comtents"
	fi
  done
	
  
}

function do_all {
  DBLIST=$(mysql $SQLPARAM -s -n --execute="SHOW DATABASES" 2>/dev/null)
  if [ ! $? = '0' ]; then
	errormsg "Could not get list of DB's"
	((error++))
  fi
  if [ ! -z $e ]; then
	exclude=$(echo $e | sed 's/,/|/g')
  else
	exclude=''
  fi

  for db in $DBLIST; do
    if echo $db | grep -E $exclude 2> /dev/null 1>/dev/null; then
    msg "excluding $db"
   else
    backup_db $db
  fi
  done

}

function selectivebackup {
    IFS=","
    for i in $b; do
      msg "starting $i backup"
      IFS=$OLDIFS
      backup_db $i
    done
    if [ ! $error = '0' ]; then
      errormsg "$error Errors found"
    fi
}


function usage {
  echo "$0 usage:"
  echo ""
  echo COMMANDS: 
  echo ---------

  echo "-a backup all db's -e to exclude specified dbs"
  echo "-b comma seperated list of db's to be backupped"
  echo ""
  echo "OPTIONS:" 
  echo "--------"
  echo "-h hostname"
  echo "-e comma seperated list op DB's to exclude"
  echo "   Default exclusions: $e"
  echo "-c cleanup in x days. Example -c 30 Default value: $KEEP_DAYS days"
  echo "-g enable gzip compression."
  echo -t Specify Targetpath. $TARGETPATH by default
  echo -v be verbose
  echo -u Username to connect
  echo -p Password to connect
  echo -C Alternative .my.cnf to use
  echo -L Specify Logfile to log to. Default is $LOGFILE
  echo " "
  echo "-I Zabbix itemname"
  echo -Z Zabbix server hostname 
  
}

###Check if targetdir is there, and if not create it
function checkdest {
  if [ ! -d $TARGETPATH ]; then
     mkdir -p $TARGETPATH
     mkstate
  fi
}

function mkstate {
  if [ ! -f $STATEFILE ]; then
	touch $STATEFILE
  fi
}
function initialize {
  OLDIFS=$IFS
  msg "The following commandline overrides where specified"
  msg "$INFOSTRING"
  now=$(date)
  ERRDB=""
  if [ $ISNFS = '1' ]; then
    checknfs
  fi
  SQLPARAM=$SQL1PARAM$SQL2PARAM
  if [ ! -z "$DO_ALL" ] && [ ! -z "$SELECTIVE_BACKUP" ]; then
      errormsg "Either specify -a or -b, not both!" 
  else
      if [ "$DO_ALL" = '1' ]; then 
        do_all
      elif [ "$SELECTIVE_BACKUP" = '1' ]; then 
        selectivebackup
      fi
  fi
 
#  if [ -z "${SRCHOST}"]; then
#    SQLPARAM="$SQLPARAM -h$SRCHOST"
#  fi

}

####BEGIN MAIN

# Check here to see if any options are passed. IF not, launch usage function
error=0; #Reset error counter
VERBOSE=0;
#SQLPARAM=$SQL1PARAM $SQL2PARAM
#check_mycnf #See if there's a ~/.my.cnf
#DO_ALL=0
#SELECTIVE_BACKUP=0

if [ $1 = ""]; then
 usage
else
  # Start parsing srgumentss
  while getopts "c:b:e:I:Z:t:au:p:L:C:gh:v" o; do
    case $o in

	      g)  #Gzip compression
	          EXT=".gz"
	          PIPE="| gzip -9"
		  INFOSTRING="$INFOSTRING Enabled GZIP compression."
	          ;;
        b)
            #Selective
            b=${OPTARG}
            #echo "S = $b"
            #initialize
            #selectivebackup
	    SELECTIVE_BACKUP=1
	    INFOSTRING="$INFOSTRING DB's to backup ${OPTARG}."
            ;;
        e)
            #Exclude
            e=$(echo $e,${OPTARG})
            #echo "P = $e"
	    INFOSTRING="$INFOSTRING Will exclude ${OPTARG}."
            ;;
        c)  ####First see if we got a number as argument otherwise error uit
            test "${OPTARG}" -eq "${OPTARG}" 2>/dev/null
            if [ $? -ne 0 ]; then
              echo "Please specify an numeric value for -c"
              exit 1
            fi
            ##############################################################
            KEEP_DAYS=${OPTARG}
	    INFOSTRING="$INFOSTRING Delete backups older than ${OPTARG} days.."
            ;;
        a)  #initialize
            #Do all DB's except exceptions
            #do_all
	    DO_ALL=1
	    INFOSTRING="$INFOSTRING Backup all DB's."
            ;;
        t)  TARGETPATH=${OPTARG}
	    INFOSTRING="$INFOSTRING Targetpath:${OPTARG}. "
            ;;
        h)  #host to connect to
            SRCHOST=${OPTARG}
            SQL2PARAM="$SQL2PARAM-h${OPTARG} "
	    INFOSTRING="$INFOSTRING Host to connect to:${OPTARG}. "
            ;;
	u)  SQL2PARAM="$SQL2PARAM-u${OPTARG} "
	    INFOSTRING="$INFOSTRING Username: ${OPTARG}. "
	    ;;
	p)  SQL2PARAM="$SQL2PARAM-p${OPTARG} "
	     INFOSTRING="$INFOSTRING Pwd specified. "
	    ;;
        v)  VERBOSE="1"
	    INFOSTRING="$INFOSTRING Verbose. "
	    ;;
	C)  SQL1PARAM="$SQL1PARAM--defaults-file=${OPTARG} "
	    INFOSTRING="$INFOSTRING Config file to use: ${OPTARG}. " 
	    ;;
	L)  LOGFILE=${OPTARG}
	    INFOSTRING="$INFOSTRING Logging to ${OPTARG}. "
	    ;;
	I)  ITEMNAME=${OPTARG}
	    INFOSTRING="$INFOSTRING Zabbix itemname: ${OPTARG}"
	    ;;
	Z)  ZABBIXSERVER=${OPTARG}
	    INFOSTRING="$INFOSTRING Zabbix servername: ${OPTARG}"
	    ;;
        \?)  usage
            ;;
        *)
            usage
            ;;
    esac
  done
  initialize
  if [ ! -z ${ERRDB} ]; then
    errormsg "Failed databases: $ERRDB"
    monitoring "Failed databases: $ERRDB"
  else
    monitoring "ALL OK"
  fi
fi


#for db in $(mysql -h$HOST -u$USER -p$PWD -s -n --execute="SHOW DATABASES WHERE \`Database\` NOT REGEXP '$IGNORE_DB'"); do
#  backup_db $db
#done.
